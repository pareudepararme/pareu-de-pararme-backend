<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->id();
            $table->string('description');
            $table->string('lat');
            $table->string('lon');
            $table->integer('confirmations')->default(0);
            $table->integer('abuses')->default(0);
            $table->string('address')->nullable();
            $table->unsignedBigInteger('alert_type_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
            
            $table->foreign('alert_type_id')->references('id')->on('alert_types');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alerts');
    }
}
