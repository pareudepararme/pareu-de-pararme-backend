<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Cop;

class CopChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $phoneId = explode('_' , $request->user()->name)[0];
        $cops = Cop::where('phoneId', $phoneId)->get();

        if (count($cops) > 0) {
            return response()->json(['message' => "Your user have been banned"],403);
        }
        return $next($request);
    }
}
