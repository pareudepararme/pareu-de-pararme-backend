<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alert;
use App\Models\AlertType;
use Carbon\Carbon;
use Google_Client;

class AlertsController extends Controller
{
    public function createAlert(Request $request) {
        $alertData = $request->validate([
            "description" => "required|string",
            "lat" => "required|numeric",
            "lon" => "required|numeric",
            "alert_type_id" => "required|integer",
            "user_created_at" => "required|integer"
        ]);
        // This make the user wait 2 minutes before creating new alerts.
        // The waiting time can be modified by editing ALERT_CREATION_WAITING_TIME in .env file
        // if (count($request->user()->alerts) > 1 ) {
        //     if($request->user()->getLatestAlert()->created_at->addMinutes(env('ALERT_CREATION_WAITING_TIME',2)) > Carbon::now())
        //     {
        //         return response()->json([
        //             "message" => "Alert Creation Limit reached, try again later",
        //         ],403);
        //     }
        // }

        $newAlert = new Alert([
            "description" => $alertData["description"],
            "lat" => $alertData["lat"],
            "lon" => $alertData["lon"],
            "alert_type_id" => $alertData["alert_type_id"],
            "user_id" => $request->user()->id,
            "user_created_at" => $alertData["user_created_at"]
        ]);
        $newAlert->save();
        $alertType = AlertType::where('id', $alertData["alert_type_id"])->get()[0];
        $nTitle = "Atencion nueva " . $alertType['alertTitle_es'];
        $nMessage = $alertData["description"];
        $this->sendNotification($nTitle,$nMessage);
        return response()->json([
            "message" => "Alert Created succesfully",
        ],200);
    }

    public function listAlerts(Request $request, $fromUserDate = null, $toUserDate = null) {
        $fromDate = Carbon::now()->subDay()->getPreciseTimestamp(3);
        $toDate = Carbon::now()->getPreciseTimestamp(3);
        if ($fromUserDate) {
            $fromDate = $fromUserDate;
        }
        if ($toUserDate) {
            $toDate = $toUserDate;
        }
        $alerts = Alert::where('user_created_at', '>' ,$fromDate)
                    ->where('user_created_at', '<' , $toDate)
                    ->get();

        $ppAlerts = [];
        foreach ($alerts as $alert ){
            $newAlert = [];
            $newAlert["description"] = $alert->description;
            $newAlert["lat"] = floatval($alert->lat);
            $newAlert["lon"] = floatval($alert->lon);
            $newAlert["user_created_at"] = $alert->user_created_at;
            // $newAlert["created_at"] = $alert->created_at->getPreciseTimestamp(3);

            //Alert Type Retrieve
            $alertType = AlertType::where('id', $alert->alert_type_id)->get()[0];
            $pAlertType = [];
            $pAlertType["keyId"] = $alertType->keyId;
            $pAlertType["icon"] = $alertType->icon;
            $pAlertType["alertTitle_cat"] = $alertType->alertTitle_cat;
            $pAlertType["alertDescription_cat"] = $alertType->alertDescription_cat;
            $pAlertType["alertTitle_es"] = $alertType->alertTitle_es;
            $pAlertType["alertDescription_es"] = $alertType->alertDescription_es;
            $newAlert["alert_type"] = $pAlertType;

            $ppAlerts[] = $newAlert;
        }
        return response()->json($ppAlerts,200);
    }

    private function getBearerToken()
    {
        $gc = new Google_Client();
        $gc->setAuthConfig(env("FIREBASE_CREDENTIALS", "/var/www/html/service-account.json"));
        $gc->addScope('https://www.googleapis.com/auth/firebase.messaging');
        $gc->fetchAccessTokenWithAssertion();
        $token = $gc->getAccessToken();
        return $token["access_token"];
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    private function sendNotification($title,$message)
    {  
        $bearerToken = $this->getBearerToken();
        // return response()->json([
        //     "message" => $bearerToken
        // ],200);
        $dataString = "{'message': {'topic': 'fcm_new_alert', 'data': {}, 'notification': {'title': '" . $title . "', 'body': '" . $message . "'}}}";
        $headers = [
            'Authorization: Bearer ' . $bearerToken,
            'Content-Type: application/json',
        ];

        $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_URL, env("FIREBASE_URL"));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
               
        $response = curl_exec($ch);
      
        curl_close($ch);
      
        return $response;
    }
}
