<?php

namespace App\Http\Controllers;

use App\Models\AlertType;
use Illuminate\Http\Request;
use App\Models\AlertTypes;

class AlertTypesController extends Controller
{
    public function getAlertType(Request $request, $id) {
        $at = AlertType::where('id', $id)->get();
        return response()->json($at[0],200);
    }

    //
    public function getAlertTypes(Request $request) {
        $at = AlertType::all();
        return response()->json($at
        ,200);
    }
}
