<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Captcha;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Ipblock;
use Illuminate\Support\Facades\Hash;
use App\Models\Cop;

class AuthenticationController extends Controller
{
    /**
     * Register
     * Registers a new user in the app
     *
     * @group Authentication
     *
     * @bodyParam captchaId string required unique phoneId of your account
     * @bodyParam phoneId string required The password of your account
     * @bodyParam captchaResolution string required unique phoneId of your account
     *
     * @response {
     *  "message": "User Created sucesfully",
     *  "username": "SN-0006_1",
     *  "password": "bb8acdbf148ee4ec3ca17b241823092e"
     * }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $alertData = $request->validate([
            "captchaId" => "required|integer",
            "captchaResolution" => "required|string",
            "phoneId" => "required|string|max:50",
        ]);

        $cId = $alertData["captchaId"];
        $cResolv = $alertData['captchaResolution'];
        $phoneId = $alertData['phoneId'];

        $cops = Cop::where('phoneId', $phoneId)->get();
        if (count($cops) > 0) {
            return response()->json(['message' => "FuckOff cop"],403);
        }

        $captcha = Captcha::where('id',$cId)->get();

        if(count($captcha) == 0) {
            return response()->json([
                "message" => "Undefined Captcha"
            ],403);
        }
        $captcha[0]->max_attempts = $captcha[0]->max_attempts + 1;
        $captcha[0]->save();
        if ($captcha[0]->max_attempts > 2) {
            return response()->json([
                "message" => "Max attempts reached for this captcha"
            ],403);
        }
        //makes Captcha resolution non case sensitive
        $uResolution = strtolower($captcha[0]->resolution);
        $cResolv = strtolower($cResolv);
        if($uResolution != $cResolv){
            return response()->json([
                "message" => "Wrong Captcha Resolution"
            ],401);
        }
        $secret = md5(Carbon::now() . $phoneId);
        
        $email = $phoneId;
        $i = 0;
        $userExists = User::where('email',$email)->get();
        while (count($userExists) != 0) {
            $email = $phoneId . "_" . strval($i);
            $userExists = User::where('email',$email)->get();
            $i++;
        }

        $user = new User([
            'name' => $email,
            'email' => $email,
            'password' => Hash::make($secret)
        ]);
        $user->save();
        $captcha[0]->delete();
        return response()->json([
            "message" => "User Created sucesfully",
            "username" => $email,
            "secret" => $secret
        ],200);
    }

    public function getRegister(Request $request) {
        $captchaImg = $this->generateCaptcha();
        $newCaptcha = new Captcha([
            "image" => $captchaImg["image"],
            "resolution" => $captchaImg["resolution"]
        ]);
        $newCaptcha->save();
        return response()->json([
            "id" => $newCaptcha->id,
            "image" => $newCaptcha->image
        ],200);
    }
    
    /**
     * Login
     * Login to your account
     *
     * @group Authentication
     *
     * @bodyParam phoneId string required unique phoneId of your account
     * @bodyParam password string required The password of your account
     *
     * @response {
     *  "accessToken": "JsonWebToken",
     *  "tokenType": "Bearer",
     *  "expires_in": "3600"
     * }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->input('phoneId'),
            'password' => $request->input('password')
        ];

        $user = User::where('email',$credentials["email"])->first();

        $phoneId = explode('_' , $credentials["email"])[0];
        $cops = Cop::where('phoneId', $phoneId)->get();
        if (count($cops) > 0) {
            return response()->json(['message' => "Your user have been banned"],403);
        }

        if (!$user) {
            return response()->json(['error' => "User does not exist"], 401);
        }
        if ($user->is_active != 1) {
            return response()->json(['error' => "Your user have been banned"], 403); 
        }
        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    /**
     * If all the auth is ok returns your user information
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser(Request $request) {
        return response()->json([
            "UserData" => $request->user()
        ]);
    }

    /**
     * Generates a captcha
     * 
     * @return base_64:png
     */
    private function generateCaptcha() {
        $width = 300;
        $height = 50;
        $font_size = 30;
        $font = env("FONTPATH" ,"/var/www/html/verdana.ttf");
        $chars_length = (5);

        $captcha_characters = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ';

        $image = imagecreatetruecolor($width, $height);
        $bg_color = imagecolorallocate($image, rand(0,255), rand(0,255), rand(0,255));
        $font_color = imagecolorallocate($image, 255, 255, 255);
        imagefilledrectangle($image, 0, 0, $width, $height, $bg_color);

        $vert_line = round($width/5);
        $color = imagecolorallocate($image, 255, 255, 255);
        for($i=0; $i < $vert_line; $i++) {
            imageline($image, rand(0,$width), rand(0,$height), rand(0,$height), rand(0,$width), $color);
        }

        $xw = ($width/$chars_length);
        $x = 0;
        $font_gap = $xw/2-$font_size/2;
        $digit = '';
        for($i = 0; $i < $chars_length; $i++) {
            $letter = $captcha_characters[rand(0, strlen($captcha_characters)-1)];
            $digit .= $letter;
            if ($i == 0) {
                $x = 0;
            }else {
                $x = $xw*$i;
            }
            imagettftext($image, $font_size, rand(-20,20), $x+$font_gap, rand(40, $height-5), $font_color, $font, $letter);
        }

        ob_start();
        imagepng($image);
        $data = ob_get_contents();
        ob_end_clean();

        return [
            "image" => base64_encode($data),
            "resolution" => $digit
        ];
    }

}
