<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AlertType;
use App\Models\User;

class Alert extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'lat',
        'lon',
        'alert_type_id',
        'user_id',
        'user_created_at'
    ];

    public function alert_type() {
        return $this->belongsTo(AlertType::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
