<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Alert;

class AlertType extends Model
{
    use HasFactory;

    public function alerts() {
        return $this->hasMany(Alert::class,'user_id');
    }
}
