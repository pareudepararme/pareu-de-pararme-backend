<?php

namespace App\Admin\Controllers;

use App\Models\Alert;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Grid\Displayers\Actions;

class AlertController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Alert';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Alert());

        $grid->column('id', __('Id'));
        $grid->column('description', __('Description'));
        $grid->column('lat', __('Lat'));
        $grid->column('lon', __('Lon'));
        $grid->column('alert_type')->display(function ($alertType) {
            return "<a href='/admin/editor/alerttypes/{$alertType['id']}' ><span class='label label-warning'>{$alertType['alertTitle_cat']}</span></a>";
        });
        $grid->column('user')->display(function ($user) {
            return "<a href='/admin/editor/users/{$user['id']}' ><span class='label label-info'>{$user['name']}</span></a>";
        });
        $grid->column('created_at', __('Created at'));
        $grid->setActionClass(Actions::class);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Alert::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('description', __('Description'));
        $show->field('lat', __('Lat'));
        $show->field('lon', __('Lon'));
        $show->field('confirmations', __('Confirmations'));
        $show->field('abuses', __('Abuses'));
        $show->field('address', __('Address'));
        $show->field('alert_type_id', __('Alert type id'));
        $show->field('user_id', __('User id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Alert());

        $form->text('description', __('Description'));
        return $form;
    }
}
