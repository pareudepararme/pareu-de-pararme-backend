<?php

namespace App\Admin\Controllers;

use App\Models\AlertType;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Grid\Displayers\Actions;

class AlertTypeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'AlertType';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AlertType());

        $grid->column('id', __('Id'));
        $grid->column('keyId', __('KeyId'));
        $grid->column('icon')->display(function ($icon) {
            return "<img style=\"max-width:35px\"src=\"data:image/png;base64, $icon \"/>";
        });
        $grid->column('alertTitle_cat', __('AlertTitle_cat'));
        $grid->column('alertTitle_es', __('alertTitle_es'));
        $grid->column('alertDescription_cat', __('alertDescription_cat'));
        $grid->column('alertDescription_es', __('alertDescription_es'));
        $grid->setActionClass(Actions::class);
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AlertType::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('keyId', __('KeyId'));
        $show->field('icon', __('Icon'));
        $show->field('alertDescription_cat', __('alertDescription_cat'));
        $show->field('alertDescription_es', __('alertDescription_es'));
        $show->field('alertTitle_cat', __('AlertTitle_cat'));
        $show->field('alertTitle_es', __('alertTitle_es'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AlertType());

        $form->text('keyId', __('KeyId'));
        $form->text('icon', __('Icon'));
        $form->text('alertDescription_cat', __('alertDescription_cat'));
        $form->text('alertDescription_es', __('alertDescription_es'));
        $form->text('alertTitle_cat', __('AlertTitle_cat'));
        $form->text('alertTitle_es', __('alertTitle_es'));

        return $form;
    }
}
