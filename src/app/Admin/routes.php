<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    // Editors
    $router->resource('/editor/users', UserController::class);
    $router->resource('/editor/alerts', AlertController::class);
    $router->resource('/editor/alerttypes', AlertTypeController::class);
    $router->resource('/editor/coplist', CopController::class);
});
