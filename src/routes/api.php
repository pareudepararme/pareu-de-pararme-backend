<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\AlertsController;
use App\Http\Controllers\AlertTypesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Public routes
Route::group(["middleware" => 'api'], function () {
    //Auth
    Route::post('login', [ AuthenticationController::class,'login']);
    Route::post('register', [AuthenticationController::class, 'register']);
    Route::get('register',[ AuthenticationController::class, 'getRegister']);
    Route::post('logout', [AuthenticationController::class ,'logout']);
    Route::post('refresh', [AuthenticationController::class ,'refresh']);
});
Route::group(["middleware" => 'api'], function () {
    //Alerts
    Route::get('getalerts',[ AlertsController::class,'listAlerts']);
    Route::get('getalerts/{fromUserDate}',[ AlertsController::class,'listAlerts']);
    Route::get('getalerts/{fromUserDate}/{toUserDate}',[ AlertsController::class,'listAlerts']);
    //AlertTypes
    Route::get('getalertstypes',[ AlertTypesController::class,'getAlertTypes']);
});

// Protected routes
Route::middleware(['auth:api','copchecker'])->group(function () {
    Route::post('createalert',[ AlertsController::class,'createAlert']);
    Route::get('user',[ AuthenticationController::class,'getUser']);
});

Route::get('send-notification', [App\Http\Controllers\NotificationController::class, 'send']);