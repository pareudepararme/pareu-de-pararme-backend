<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .center {
                float:left;
                width:50%;
                margin-left:25%;
                text-align:center;
            }

            .footerlinks {
                float:left;
                text-align: center;
                width: 33%;
                max-height: 20px;
                font-size: 20px;
            }
        </style>
        <title>Pareu de pararme</title>
    </head>
    <body style="background: url(img/banner-background.jpg);background-size: cover;" >
        <a href="https://pareudepararme.org" >
            <div class="center">
                <img alt="background" src="img/banner-text.png" >
            </div>
        </a>
        <div class="center">
            <div class="footerlinks">
                <svg class="icons" width="20px" height="20px" enable-background="new 0 0 512 512" version="1.1" viewBox="0 0 512 512" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><polygon points="499.81 128.94 499.81 454.83 12.193 454.83 12.193 128.94 256 92.348" fill="#EBEDEC"/><rect x="418.85" y="57.168" fill="#EE3840"/><rect x="12.193" y="55.948" width="406.65" height="71.775" fill="#4C5870"/><g fill="#11113F"><path d="m162.53 275.38h181.76c6.737 0 12.198-5.461 12.198-12.198s-5.461-12.198-12.198-12.198h-181.76c-6.737 0-12.198 5.461-12.198 12.198-1e-3 6.737 5.461 12.198 12.198 12.198z"/><path d="m176.2 308.4h-59.114c-6.737 0-12.198 5.461-12.198 12.198s5.461 12.198 12.198 12.198h59.114c6.737 0 12.198-5.461 12.198-12.198 1e-3 -6.737-5.461-12.198-12.198-12.198z"/><path d="m285.56 308.4h-59.114c-6.737 0-12.198 5.461-12.198 12.198s5.461 12.198 12.198 12.198h59.114c6.737 0 12.198-5.461 12.198-12.198s-5.461-12.198-12.198-12.198z"/><path d="m394.91 308.4h-59.114c-6.737 0-12.198 5.461-12.198 12.198s5.461 12.198 12.198 12.198h59.114c6.737 0 12.198-5.461 12.198-12.198s-5.461-12.198-12.198-12.198z"/><path d="m499.8 44.97h-487.6c-6.737 0-12.198 5.461-12.198 12.198v397.66c0 6.737 5.461 12.198 12.198 12.198h487.6c6.737 0 12.198-5.461 12.198-12.198v-397.66c1e-3 -6.737-5.46-12.198-12.197-12.198zm-12.199 71.779h-56.552v-47.383h56.554v47.383h-2e-3zm-80.949-47.383v47.383h-382.26v-47.383h382.26zm-382.26 373.27v-301.49h463.21v301.49h-463.21v1e-3z"/></g></svg>
                <a href="https://www.pareudepararme.org/">Pagina Oficial</a>
            </div>

            <div class="footerlinks">
            <svg class="icons" width="20px" height="20px" enable-background="new 0 0 512 512" version="1.1" viewBox="0 0 512 512" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><polygon points="499.81 128.94 499.81 454.83 12.193 454.83 12.193 128.94 256 92.348" fill="#EBEDEC"/><rect x="418.85" y="57.168" fill="#EE3840"/><rect x="12.193" y="55.948" width="406.65" height="71.775" fill="#4C5870"/><g fill="#11113F"><path d="m162.53 275.38h181.76c6.737 0 12.198-5.461 12.198-12.198s-5.461-12.198-12.198-12.198h-181.76c-6.737 0-12.198 5.461-12.198 12.198-1e-3 6.737 5.461 12.198 12.198 12.198z"/><path d="m176.2 308.4h-59.114c-6.737 0-12.198 5.461-12.198 12.198s5.461 12.198 12.198 12.198h59.114c6.737 0 12.198-5.461 12.198-12.198 1e-3 -6.737-5.461-12.198-12.198-12.198z"/><path d="m285.56 308.4h-59.114c-6.737 0-12.198 5.461-12.198 12.198s5.461 12.198 12.198 12.198h59.114c6.737 0 12.198-5.461 12.198-12.198s-5.461-12.198-12.198-12.198z"/><path d="m394.91 308.4h-59.114c-6.737 0-12.198 5.461-12.198 12.198s5.461 12.198 12.198 12.198h59.114c6.737 0 12.198-5.461 12.198-12.198s-5.461-12.198-12.198-12.198z"/><path d="m499.8 44.97h-487.6c-6.737 0-12.198 5.461-12.198 12.198v397.66c0 6.737 5.461 12.198 12.198 12.198h487.6c6.737 0 12.198-5.461 12.198-12.198v-397.66c1e-3 -6.737-5.46-12.198-12.197-12.198zm-12.199 71.779h-56.552v-47.383h56.554v47.383h-2e-3zm-80.949-47.383v47.383h-382.26v-47.383h382.26zm-382.26 373.27v-301.49h463.21v301.49h-463.21v1e-3z"/></g></svg>
                <a href="https://www.pareudepararme.org/manifest/">Manifest</a>
            </div>

            <div class="footerlinks">
                <svg class="icons" width="20px" height="20px" viewBox="0 0 256 236" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet"><path d="M128.075 236.075l47.104-144.97H80.97l47.104 144.97z" fill="#E24329"/><path d="M128.075 236.074L80.97 91.104H14.956l113.119 144.97z" fill="#FC6D26"/><path d="M14.956 91.104L.642 135.16a9.752 9.752 0 0 0 3.542 10.903l123.891 90.012-113.12-144.97z" fill="#FCA326"/><path d="M14.956 91.105H80.97L52.601 3.79c-1.46-4.493-7.816-4.492-9.275 0l-28.37 87.315z" fill="#E24329"/><path d="M128.075 236.074l47.104-144.97h66.015l-113.12 144.97z" fill="#FC6D26"/><path d="M241.194 91.104l14.314 44.056a9.752 9.752 0 0 1-3.543 10.903l-123.89 90.012 113.119-144.97z" fill="#FCA326"/><path d="M241.194 91.105h-66.015l28.37-87.315c1.46-4.493 7.816-4.492 9.275 0l28.37 87.315z" fill="#E24329"/></svg>
                <a href="https://gitlab.com"> Codigo del proyecto</a>
            </div>
        </div>
        <div class="center">
            <a href="https://play.google.com/store" >
                <img style="max-width:220px" src="img/googleplay_logo.png"  /> 
            </a>
        </div>
    </body>
</html>
