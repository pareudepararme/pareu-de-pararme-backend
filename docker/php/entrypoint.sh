#!/usr/bin/env bash

php /var/www/html/artisan queue:work --tries=3 &

chown -R www-data:www-data /var/www/html/storage
chmod -R 777 /var/www/html/storage

docker-php-entrypoint php-fpm
